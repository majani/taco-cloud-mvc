package com.example.spring.singer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.spring.singer.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String user);

}
