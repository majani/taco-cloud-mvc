package com.example.spring.singer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.spring.singer.models.Order;


@CrossOrigin("*")
public interface OrderRepository extends JpaRepository<Order, Long>{

}
