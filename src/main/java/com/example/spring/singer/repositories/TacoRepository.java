package com.example.spring.singer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.spring.singer.models.Taco;

public interface TacoRepository extends JpaRepository<Taco, Long> {
}
