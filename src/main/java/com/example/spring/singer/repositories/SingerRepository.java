package com.example.spring.singer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.spring.singer.models.Singer;

public interface SingerRepository extends JpaRepository<Singer, Long> {

}
