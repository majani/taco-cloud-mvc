package com.example.spring.singer.service;

import java.util.List;


import com.example.spring.singer.models.Singer;

public interface SingerService {
	List<Singer> findAllSingers();

	Singer findSinger(Long id);

	void saveSinger(Singer singer);


}
