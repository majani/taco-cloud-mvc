package com.example.spring.singer.service;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SerializerMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.example.spring.singer.models.Taco;

@Service
public class TacoRabbit implements TacoMessagingService {

	@Autowired
	RabbitTemplate rabbit;

//	@Bean
//	public MessageConverter converter() {
//		return new SerializerMessageConverter();
//	}

	@Override
	public void sendTaco(Taco taco) {
		MessageConverter convert = rabbit.getMessageConverter();
		MessageProperties props = new MessageProperties();
		Message message = convert.toMessage(taco, props);
		rabbit.send("tacocloud.tacos", "", message);

	}

}
