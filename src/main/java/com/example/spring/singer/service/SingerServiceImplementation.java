package com.example.spring.singer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.example.spring.singer.models.Singer;
import com.example.spring.singer.repositories.SingerRepository;

@Service
@Primary
public class SingerServiceImplementation implements SingerService {

	@Autowired
	SingerRepository singerRepo;

	@Override
	public List<Singer> findAllSingers() {
		return singerRepo.findAll();
	}

	@Override
	public Singer findSinger(Long id) {
		return singerRepo.findById(id).get();
	}

	@Override
	public void saveSinger(Singer singer) {
		singerRepo.save(singer);
	}

}
