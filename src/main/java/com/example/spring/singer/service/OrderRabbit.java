package com.example.spring.singer.service;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.example.spring.singer.models.Order;

@Service
@Primary
public class OrderRabbit implements OrderMessagingService {
	
	@Autowired
	private RabbitTemplate rabbit;
	
	public void sendOrder(Order order) {
		MessageConverter converter = rabbit.getMessageConverter();
		MessageProperties props = new MessageProperties();
		Message message = converter.toMessage(order, props);
		rabbit.send("tacocloud.orders", message);
	}

}
