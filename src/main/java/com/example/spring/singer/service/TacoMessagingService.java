package com.example.spring.singer.service;

import com.example.spring.singer.models.Taco;

public interface TacoMessagingService {
	
	void sendTaco(Taco taco);

}
