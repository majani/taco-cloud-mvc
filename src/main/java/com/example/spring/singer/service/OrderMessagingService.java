package com.example.spring.singer.service;

import com.example.spring.singer.models.Order;

public interface OrderMessagingService {
	void sendOrder(Order order);

}
