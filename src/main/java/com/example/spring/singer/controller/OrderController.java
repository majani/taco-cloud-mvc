package com.example.spring.singer.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;

import com.example.spring.singer.models.Order;
import com.example.spring.singer.models.User;
import com.example.spring.singer.repositories.OrderRepository;
import com.example.spring.singer.service.OrderMessagingService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/orders")
@Slf4j
public class OrderController {

	@Autowired
	OrderRepository orderRepo;

	@Autowired
	RabbitTemplate rabbit;

	@GetMapping("/current")
	public String orderForm(Model model) {
		model.addAttribute("order", new Order());
		return "orderForm";

	}

	@PostMapping
	public String processOrder(@Valid Order order, Errors errors, SessionStatus sessionStatus,
			@AuthenticationPrincipal User user) {
		if (errors.hasErrors()) {
			return "orderForm";
		}

		order.setUser(user);
		log.info("Submitted " + order);
		rabbit.convertAndSend("tacocloud.orders.queue", order);
		log.info("SENT ORDER TO RABBIT");
		orderRepo.save(order);
		sessionStatus.setComplete();
		return "redirect:/design";
	}

}
