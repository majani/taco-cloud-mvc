package com.example.spring.singer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.spring.singer.models.Singer;
import com.example.spring.singer.service.SingerService;

@Controller
@RequestMapping("/singers")
public class SingerController {

	@Autowired
	SingerService singerSvc;

	@GetMapping("list")
	public String listSingers(Model uiModel) {
		List<Singer> singers = singerSvc.findAllSingers();
		uiModel.addAttribute("singers", singers);
		return "list";
	}

	@ModelAttribute(name = "singer")
	public Singer singer() {
		return new Singer();
	}

	@GetMapping
	public String showAddSinger() {
		return "create";

	}

	@PostMapping
	public String createPost(Singer singer, BindingResult result) {
		if (result.hasErrors()) {
			return "create";
		}

		singerSvc.saveSinger(singer);
		return "redirect:/singers/list";
	}

}
