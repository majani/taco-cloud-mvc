package com.example.spring.singer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.spring.singer.models.User;
import com.example.spring.singer.repositories.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/register")
@Slf4j
public class RegistrationController {

	@Autowired
	UserRepository userRepo;

	@Autowired
	BCryptPasswordEncoder encoder;

	@GetMapping
	public String registerForm(@ModelAttribute("user") User user) {
		return "registration";
	}

	@PostMapping
	public String processRegistration(@ModelAttribute("user") @Valid User user, BindingResult result) {

		if (result.hasErrors()) {
			log.info("Validation failed", result);
			return "registration";
		}

		String pwd = user.getPassword();
		String encodedPwd = encoder.encode(pwd);
		user.setPassword(encodedPwd);
		userRepo.save(user);
		return "redirect:/login";
	}

}
