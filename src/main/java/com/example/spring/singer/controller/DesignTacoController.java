package com.example.spring.singer.controller;

import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.example.spring.singer.models.Ingredient;
import com.example.spring.singer.models.Order;
import com.example.spring.singer.models.Taco;
import com.example.spring.singer.models.Ingredient.Type;
import com.example.spring.singer.repositories.IngredientRepository;
import com.example.spring.singer.repositories.TacoRepository;
import com.example.spring.singer.service.TacoMessagingService;

import lombok.extern.slf4j.Slf4j;

@RequestMapping("/design")
@Controller
@Slf4j
@SessionAttributes("order")
public class DesignTacoController {

	@Autowired
	private IngredientRepository ingredientRepo;

	@Autowired
	RabbitTemplate rabbit;

	@Autowired
	private TacoRepository designRepo;

	@GetMapping
	public String showDesignForm(Model model) {
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		ingredientRepo.findAll().forEach(i -> ingredients.add(i));

		Type[] types = Ingredient.Type.values();

		for (Type type : types) {
			model.addAttribute(type.toString().toLowerCase(), filterByType(ingredients, type));
		}
		model.addAttribute("design", new Taco());

		return "design";
	}

	@ModelAttribute(name = "taco")
	public Taco taco() {
		return new Taco();
	}

	@ModelAttribute(name = "order")
	public Order order() {
		return new Order();
	}

	@PostMapping
	public String processDesign(@Valid Taco design, Errors errors, @ModelAttribute Order order) {

		if (errors.hasErrors()) {
			return "/design";
		}

		log.info("Processing design");
		log.info("{}", errors);

		Taco saved = designRepo.save(design);
		// rabbit.sendTaco(design);
		rabbit.convertAndSend("tacocloud.tacos.queue", design);
		log.info("SENT TACO TO RABBIT");
		order.addDesign(saved);
		return "redirect:/orders/current";

	}

	@GetMapping("/list")
	public String listTacos(Model model) {
		List<Taco> tacos = new ArrayList<Taco>();
		designRepo.findAll().forEach(i -> tacos.add(i));
		model.addAttribute("tacos", tacos);
		return "list";

	}

	private List<Ingredient> filterByType(List<Ingredient> ingredients, Type type) {

		return ingredients.stream().filter(x -> x.getType().equals(type)).collect(Collectors.toList());

	}

	@Bean
	public MessageConverter converter() {
		return new Jackson2JsonMessageConverter();
	}

}
