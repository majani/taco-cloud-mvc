//package com.example.spring.singer.models;
//
//import javax.validation.constraints.NotNull;
//
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
//import lombok.Data;
//
//@Data
//public class RegistrationForm {
//
//	@NotNull(message = "Please fill in Username")
//	private String username;
//	@NotNull(message = "Please Fill in Password")
//	private String password;
//	@NotNull(message = "Please Fill In Full Name")
//	private String fullname;
//	@NotNull(message = "Please Fill In Street Address")
//	private String street;
//	@NotNull(message = "Please Fill In City")
//	private String city;
//	@NotNull(message = "Please Fill In Your State")
//	private String state;
//	private String zip;
//	@NotNull(message = "Please Fill In your Phone Number")
//	private String phoneNumber;
//
//	public User toUser(BCryptPasswordEncoder passwordEncoder) {
//		return new User(null, username, passwordEncoder.encode(password), fullname, street, city, state, zip, phoneNumber);
//
//	}
//
//}
