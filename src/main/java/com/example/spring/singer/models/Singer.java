package com.example.spring.singer.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@RequiredArgsConstructor
public class Singer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final Long id;
	@Version
	private final int version;
	@NotNull(message = "Please fill in first name")
	@Size(min = 3, max = 60, message = "Name has a minimum length of 3 and a max of 60")
	private final String firstName;
	@NotNull(message = "Please fill in last name")
	@Size(min = 3, max = 60, message = "Name has a minimum length of 3 and a max of 60")
	private final String lastName;
	@Temporal(TemporalType.DATE)
	private final Date birthDate;
	private final String description;

}
