package com.example.spring.singer.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "Taco_Order")
@Data
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Date placedAt;
	@NotBlank(message = "Name is required")
	@Column(name = "delivery_name")
	private String name;

	@NotBlank(message = "Please fill up Street")
	@Column(name = "delivery_street")
	private String street;

	@NotBlank(message = "Please fill in City")
	@Column(name = "delivery_city")
	private String city;

	@NotBlank(message = "Please fill in the State")
	@Column(name = "delivery_state")
	private String state;

	@NotBlank(message = "Please fill in Zip")
	@Column(name = "delivery_zip")
	private String zip;

//	@CreditCardNumber(message = "Enter a valid credit card number")
	@Digits(integer = 16, fraction = 0, message = "Enter a valid credit card number")
	private String ccNumber;

	@Pattern(regexp = "^(0[1-9]|1[0-2])([\\/])([1-9][0-9])$", message = "Must be formatted MM/YY")
	private String ccExpiration;

	@Digits(integer = 3, fraction = 0, message = "Invalid CVV")
	@Column(name = "cc_cvv")
	private String ccCVV;

	// Orders belong to One user. A user can have many orders
	@ManyToOne
	private User user;

	@ManyToMany(targetEntity = Taco.class)
	private List<Taco> tacos = new ArrayList<>();

	public void addDesign(Taco design) {
		this.tacos.add(design);
	}

	@PrePersist
	void placedAt() {
		this.placedAt = new Date();
	}

}
